<?php

use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../model/Seance.php");
include_once(__DIR__ ."/../model/User.php");
include_once(__DIR__ ."/../model/Database.php");

final class UserTest extends TestCase{
    
    public function testCreateUser(){
        $user = User::createUser("Marty" , "marty@gmail.com" , password_hash("1234" , PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));

        $database = new Database();

        $this->assertNotFalse($database->createUser($user));
    }

public static function tearDownAfterClass(){
    $database = new Database();
    $database->deleteAllInscrit();
    $database->deleteAllUser();
    $database->deleteAllSeance();
}

public function testGetAndActivateUser(){
    $database = new Database();
    $user =User::createUser("Marty" , "marty@gmail.com" , password_hash("1234" , PASSWORD_DEFAULT),
                             0, 0, bin2hex(random_bytes(20)));
    $id = $database->createUser($user);
    $this->assertNotFalse($id);
    $this->assertTrue($database->activateUser($id));
    $user = $database->getUserById($id);
    $this->assertInstanceOf(User::class, $user);
    $this->assertEquals(1, $user->isActif());
}

public function testEmailAlreadyExists(){
    $database = new Database();
    $user =User::createUser("Marty" , "marty@gmail.com" , password_hash("1234" , PASSWORD_DEFAULT),
                             0, 0, bin2hex(random_bytes(20)));
    $this->assertNotFalse($database->createUser($user));
    $emailTrue = "marty@gmail.com";
    $this->assertTrue($database->isEmailExists($emailTrue));
    $emailFalse = "marty@ghotail.com";
    $this->assertFalse($database->isEmailExists($emailFalse));
}

public function testGetUserByEmail(){
    $database = new Database();
    $user = User::createUser("Marty" , "marty@gmail.com" , password_hash("1234" , PASSWORD_DEFAULT),
    0, 0, bin2hex(random_bytes(20)));
    $this->assertNotFalse($database->createUser($user));
    $emailTrue = "marty@gmail.com";
    $this->assertInstanceOf(User::class, $database->getUserByEmail($emailTrue));
    $emailFalse = "marty@ghotail.com";
    $this->assertFalse($database->getUserByEmail($emailFalse));
}



}
?>