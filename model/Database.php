<?php
/**
 * Classe Database pour intéragir avec la base de données
 */

include_once "Seance.php";
include_once "User.php";
/**
 * Classe qui fait l'interface entre la vue et la base de données
 */

class Database
{
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD ="W79meDk5";

    // Attribut privé qui va contenir la connexion à la BD
    private $connexion;

    // On crée la connexion dans le constructeur
    public function __construct(){
        try {
       // On instancie un nouvel objet PDO
        $this->connexion = new
            PDO("mysql:host=" .self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                                    self::DB_USER,
                                    self::DB_PASSWORD);
                            
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' .$e->getMessage();
        }
     }
/*

        @param{Seance}
        @return()
*/
    // Getter pour pouvoir tester ce que contient la variable connexion
    public function createSeance(Seance $seance){
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
             VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        $pdoStatement->execute([
            "titre"                 => $seance->getTitre(),
            "description"           => $seance->getDescription(),
            "heureDebut"            => $seance->getHeureDebut(),
            "date"                  => $seance->getDate(),
            "duree"                 => $seance->getDuree(),
            "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
            "couleur"               => $seance->getCouleur()
        ]);
       //var_dump($pdoStatement->errorInfo());
       if($pdoStatement->errorCode() == 0){
           $id = $this->connexion->lastInsertId();
           return $id;
       }else{
           return false;
       }
    }

    public function getSeanceById($id){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        $pdoStatement->execute(
            ["id" => $id]
        );
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }
    
    public function getSeanceByWeek($week){
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE WEEKOFYEAR(date)= :week"
        );
            $pdoStatement->execute(
                ["week" => $week]
            );
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        return $seances;
    }
    public function deleteAllSeance(){
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }

public function updateSeance(Seance $seance){
    $pdoStatement = $this->connexion->prepare(
        "UPDATE seances
        SET titre = :titre, description = :description, heureDebut = :heureDebut,
            date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
            WHERE id = :id"
    );
    $pdoStatement->execute([
        "titre"                 => $seance->getTitre(),
        "description"           => $seance->getDescription(),
        "heureDebut"            => $seance->getHeureDebut(),
        "date"                  => $seance->getDate(),
        "duree"                 => $seance->getDuree(),
        "nbParticipantsMax"     => $seance->getNbParticipantsMax(),
        "couleur"               => $seance->getCouleur(),
        "id"                    => $seance->getId()
    ]);
    //var_dump($pdoStatement->errorInfo());

    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
}

public function deleteSeance($id){
    $pdoStatement = $this->connexion->prepare(
        "Delete FROM inscrits WHERE id_seance = :seance"
    );
    $pdoStatement->execute(
        ["seance" => $id]
    );
    if($pdoStatement->errorCode() != 0){
        return false;
    }
    $pdoStatement = $this->connexion->prepare(
        "DELETE FROM seances WHERE id = :seance"
    );
    $pdoStatement->execute(
        ["seance" => $id]
    );
    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
}
public function insertParticipant($idSeance, $idUser){
    $pdoStatement = $this->connexion->prepare(
        "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
    );
    $pdoStatement->execute(
        ["id_user" => $idUser,
        "id_seance" => $idSeance]
    );
    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
}

public function deleteParticipant($idSeance, $idUser){
    $pdoStatement = $this->connexion->prepare(
    "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
    );
    $pdoStatement->execute(
        ["id_user" => $idUser,
        "id_seance" => $idSeance]
);
    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
}

// fonction nouveau user suvegarde BD
public function createUser(User $user){
    $pdoStatement = $this->connexion->prepare(
        "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
        VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
    );
    $pdoStatement->execute([
        "nom"           => $user->getNom(),
        "email"         => $user->getEmail(),
        "password"      => $user->getPassword(),
        "isAdmin"       => $user->isAdmin(),
        "isActif"       => $user->isActif(),
        "token"         => $user->getToken()
    ]);
    if($pdoStatement->errorCode() == 0){
        $id = $this->connexion->lastInsertId();
        return $id;
    }else{
        return false;
    }
}

public function deleteAllUser(){
    $pdoStatement = $this->connexion->prepare(
        "DELETE FROM inscrits;"
    );
    $pdoStatement->execute();
}

public function getUserById($id){
    $pdoStatement = $this->connexion->prepare(
        "SELECT * FROM users WHERE id = :id"
    );
    $pdoStatement->execute(
        ["id" => $id]
    );
    $user = $pdoStatement->fetchObject("User");
    return $user;
}

public function activateUser($id){
    $pdoStatement = $this->connexion->prepare(
        "UPDATE users
        SET isActif = 1
        WHERE id = :id"
    );
    $pdoStatement->execute([
        "id" => $id
    ]);
    if($pdoStatement->errorCode() == 0){
        return true;
    }else{
        return false;
    }
}
public function deleteAllInscrit(){
    $pdoStatement = $this->connexion->prepare(
        "DELETE FROM inscrits"
    );
    $pdoStatement->execute();
}

public function isEmailExists($email){
    $pdoStatement = $this->connexion->prepare(
        "SELECT COUNT(*) FROM users WHERE email = :email"
    );
    $pdoStatement->execute(
        ["email" => $email]
    );
    $nbUser = $pdoStatement->fetchColumn();
    if($nbUser == 0){
        return false;
    }else{
        return true;
    }
}

public function getUserByEmail($email){
    $pdoStatement = $this->connexion->prepare(
        "SELECT * FROM users WHERE email = :email"
    );
    $pdoStatement->execute(
        ["email" => $email]
    );
    $user = $pdoStatement->fetchObject("User");
    return $user;
}


public function getSeanceByUserId($idUser){
    $pdoStatement = $this->connexion->prepare(
        "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user"
    );
    $pdoStatement->execute(
        ["id_user" => $idUser]
    );
    $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
    return $seances;
}



}

    ?>

