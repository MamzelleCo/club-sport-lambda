<?php

class User {

    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

    public function __construct(){}

    public static function createUser($nom, $email, $password, $isAdmin, $isActif, $token){
        $user = new self();
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setAdmin($isAdmin);
        $user->setActif($isActif);
        $user->setToken($token);
       // var_dump($user);
        return $user;
    }

    //-Getters-->
public function getId(){ return $this->id; }
public function getNom(){ return $this->nom; }
public function getEmail(){ return $this->email; }
public function getPassword(){ return $this->password; }
public function isAdmin(){ return $this->isAdmin; }
public function isActif(){ return $this->isActif; }
public function getToken(){ return $this->token; }

//Setters-->
public function setId($id){ $this->id = $id; }
public function setNom($nom){ $this->nom= $nom; }
public function setEmail($email){ $this->email= $email; }
public function setPassword($password){ $this->password = $password; }
public function setAdmin($admin){ $this->isAdmin = $admin; }
public function setActif($actif){ $this->isActif = $actif; }
public function setToken($token){ $this->token = $token; }

}