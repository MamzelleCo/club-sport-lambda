<?php
include('modules/partie1.php');
?>

<div class="container card text-center mt-4 sm-12">
  <div class="card-body">
    <h1 class="card-title text-center">Club Lambda</h1>
    <h3 class="card-subtitle text-center col-sm-12 mb-2 mt-2 text-muted">Bienvenue</h6>
    <img class="mainImage mt-2 mb-2" src="/vues/assets/images/fitness.jpg" alt="photo de plusieurs personnes faisant du vélo/fitness">
    <p class="card-text mt-2">Aux États-Unis. <br>
        Le fitness trouve ses origines dans l'aérobic (gymnastique modelant le corps par des mouvements effectués en musique), qui lui-même naît de la danse jazz.
        En 1968, le docteur en médecine Kenneth H. Cooper (en), lieutenant-colonel dans l'armée de terre des États-Unis, élabore un concept d'activités physiques visant à apporter un gain de forme (reposant sur une activation du système cardio-vasculaire) à ses pratiquants. Afin de prouver l'utilité de ses exercices, il publie un ouvrage intitulé Aérobic.
        Dans les années 1970, le mouvement aérobic se développe, se consolide et surtout devient médiatique sous l'impulsion de Jane Fonda. Celle-ci crée sa propre méthode de travail qu'elle nomme Work Out.
        En 1986, Gin Miller crée les cours d'une composante majeure du fitness : le step.
        Dans les années 1990, les BTS (body training system) font leur apparition et donnent lieu à ce que l'on appelle : body pump, body balance, body combat, body attack, body jam…
       <br> En France. <br>
        Dans les années 1980, le fitness arrive en France. Véronique et Davina sont les précurseuses d'un nouveau mouvement qui prend de l'ampleur. C'est l'émission télévisée Gym Tonic qui les révèle au grand public et qui apporte le fameux tube intergénérationnel : toutouyoutou.
        En France, à travers les exigences de la clientèle et des nouvelles structures de remise en forme, l'évolution du fitness dans les années 2000 a eu pour incidence la venue d'une clientèle plus variée, grâce à des créneaux plus adaptés, accessibles et nombreux et à des activités toujours plus nombreuses, en plus des activités traditionnelles. L'enseignement s'est également professionnalisé. Le métier a évolué et des services personnalisés se sont développés (mentorat ou conseil individuel). Cependant, un bon nombre de structures indépendantes ont fait faillite. L'arrivée des grandes enseignes, les franchises, est un facteur d'explication.
        En 2016, en France, le marché du fitness, et plus généralement des sports en salle, affiche une croissance supérieure à la moyenne européenne, avec seulement 36 % de français « non-sportifs »3. </p>
    <a class="btn btn-dark" href="modules/planning.php">Consulter le planning</a>
    </div>
</div>

<?php
include('modules/partie3.php');
?>