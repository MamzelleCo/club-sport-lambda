<?php
include('modules/partie1.php')
?>


<?php
require_once(__DIR__ ."/../model/Database.php");

const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

$idSeanceOrigine = $_GET["id"];
$type = $_GET["type"];

if(isset($idSeanceOrigine)){
    $database = new Database();
    $seanceOrigine = $database->getSeanceById($idSeanceOrigine);
}

$seance = new Seance();
$titre = "";
switch($type){
    case CREATION :
        $titre = "Création d'une séance";
    break;
    case MODIFICATION :
        $seance = $seanceOrigine;
        $titre = "Modification de ";
    break;
    case DUPLICATION :
        $seance->setTitre($seanceOrigine->getTitre());
        $seance->setCouleur($seanceOrigine->getCouleur());
        $seance->setDate($seanceOrigine->getDate());
        $seance->setHeureDebut($seanceOrigine->getHeureDebut());
        $seance->setDuree($seanceOrigine->getDuree());
        $seance->setDescription($seanceOrigine->getDescription());
        $seance->setNbParticipantsMax($seanceOrigine->getNbParticipantsMax());
        $titre = "Duplication de ";
    break;
default:
$titre = "Création d'une séance";    

}
?>


<div class="container card text-center mt-4">
    <h1 class="card-header"><?php echo $titre.$seance->getTitre(); ?></h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="/process/formulaire.php" method="POST">
        <input type="hidden" name="type" value="<?php echo $type?>"> 
        <input type="hidden" name="id" value="<?php echo $seance->getId(); ?>"> 
            <div class="form-group row">
                <label for="nom" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom du cours" value="<?php echo $seance->getTitre(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="couleur" class="col-sm-12col-md-4 col-form-label">Couleur de fond</label>
                <div class="col-sm-12 col-md-8">
                    <input type="color" class="form-control" id="couleur" name="couleur" placeholder="Couleur" value="<?php echo $seance->getCouleur(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-12col-md-4 col-form-label">Date</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="date" name="date" value="<?php echo $seance->getDate(); ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="heureDebut" class="col-sm-12col-md-4 col-form-label">Heure de début</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" value="<?php echo $seance->getHeureDebut(); ?>" required>
                    <span>minutes</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="heureDebut" class="col-sm-12col-md-4 col-form-label">Durée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="duree" name="duree" value="<?php echo $seance->getDuree(); ?>" required>
                    <span>minutes</span>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-12col-md-4 col-form-label">description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" name="description" id="description"><?php echo $seance->getDescription(); ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="nbParticipants" class="col-sm-12col-md-4 col-form-label">Nombre de participants max</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="nbParticipants" name="nbParticipants" value="<?php echo $seance->getNbParticipantsMax(); ?>" required>
                    <span>participants</span>
                </div>
            </div>
            <div class="form-group text-center">
                    <button class="btn btn-dark" type="submit"><?php
                    switch($type){
                        case CREATION : echo "Crée"; break;
                        case MODIFICATION : echo "Modifier"; break;
                        case DUPLICATION : echo "Duplication"; break;
                    }
                    ?>
                    </button>
            </div>
        </form>         
    </div>
</div>

<?php
include('modules/partie3.php')
?>