<footer>

    <div class="container-fluid bg-info text-white pt-2">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <h5>Quick links</h5>
  
                    <ul class="list-unstyled">
                    <li><a href="#club">Home</a></li>
                    <li><a href="#club">About</a></li>
                    <li><a href="#!">FAQ</a></li>
                    <li><a href="#!">Get Started</a></li>
                    <li><a href="#!">Vidéos</a></li>
                    </ul>
             </div>
            </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
            <p class="h6">&copy All right Reserved. <a class="ml-2" href="https://realise.ch"
            target="_blank">Realise</a></p>
            </div>
             </hr>
         </div>
     </div>
</footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
