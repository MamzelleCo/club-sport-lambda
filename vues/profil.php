<?php
include('modules/partie1.php')
?>

<div class="container inscrit mt-3">
  <h1>Bienvenue Marty McFly</h1>
  <p>Vous êtes inscrit aux cours suivants :</p>
  <ul class="list-group">
    <li class="list-group-item d-flex justify-content-between align-items-center">
      Pilates, le 10/01/2020
      <span class="badge badge-primary badge-pill">9h</span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
     Pilates, le 17/01/2020
      <span class="badge badge-primary badge-pill">9h</span>
    </li>
    <li class="list-group-item d-flex justify-content-between align-items-center">
      Yoga, le 22/01/2020
      <span class="badge badge-primary badge-pill">10h</span>
    </li>
  </ul>
</div>

<?php
include('modules/partie3.php')
?>