<?php
session_start();

require_once(__DIR__ ."/../model/Database.php");
$database = new Database();

const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

$titre = isset($_POST["titre"]) ? $_POST["titre"] : "";
$description = isset($_POST["description"]) ? $_POST["description"] : "";
$heureDebut = isset($_POST["heureDebut"]) ? $_POST["heureDebut"] : "00:00";
$date = isset($_POST["date"]) ? $_POST["date"] : date("Y-m-d");
$duree = isset($_POST["duree"]) ? $_POST["duree"] : "45";
$nbParticipants = isset($_POST["nbParticipants"]) ? $_POST["nbParticipants"] : "10";
$couleur = isset($_POST["couleur"]) ? $_POST["couleur"] : "#ffffff";

$type = isset($_POST["type"]) ? $_POST["type"] : CREATION;
$id = isset($_POST["id"]) ? $_POST["id"] : null;

$seance = Seance::createSeance($titre, $description, $heureDebut, $date, $duree, $nbParticipants, $couleur);

$error = null;
$succes = null;

switch($type){
    case CREATION :
    case DUPLICATION :
        $id = $database->createSeance($seance);
        if($id){
            $succes = "La séance a été créée avec succès";
        }else{
            $error = "La création de la séance a rencontré une erreur";
        }
        break;
        case MODIFICATION :
            $seance->setId($id);
            if($database->updateSeance($seance)){
                $succes = "La séance a été modifiée avec succès";
            }else{
                $error = "La modification de la séance a échoué";
            }
            default :
}
if($error == null){
    $_SESSION["info"] = $succes;
    header("Location: ../vues/cours.php?id=".$id);
}else{
    $_SESSION["error"] = $error;
    header("location: ../vues/formulaire.php?id".$id."&type=".$type);
}
?>