<?php

session_start();

require_once(__DIR__ ."/../model/Database.php");
$database = new Database();

$idSeance = $_GET["id"];

if(!$idSeance){
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/planning.php");
}
if($database->deleteSeance($idSeance)){
    $_SESSION["info"] = "Séance supprimée avec succès";
    header("location: ../vues/planning.php");
}else{
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/cours.php?id=".$id);
}


?>